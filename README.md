ExtendJ print-assign operator extension
======================

This project is an extension to the Java compiler ExtendJ in which the print-assign (.=) and spread-dot (\*.) operators are implemented.

The print-assign operator is a decorator of the normal assign, such that it also prints the name and new value of the assigned variable.

The spread-dot operator is a simplification of the one existing in [Groovy](http://docs.groovy-lang.org/latest/html/documentation/#_spread_operator). The one implemented in this project only supports operations on arrays, and no chaining of dots and spread-dots. See the demo code for what is implemented.

Cloning this Project
--------------------

To clone this project you will need [Git][3] installed.

Use this command to clone the project using Git:

    git clone --recursive https://bitbucket.org/edan70/2017-operators-filip-wavve.git

The `--recursive` flag makes Git also clone the ExtendJ submodule while cloning
the `extension-base` repository.

If you forgot the `--recursive` flag you can manually clone the ExtendJ
submodule using these commands:

    cd extension-base
    git submodule init
    git submodule update

This should download the ExtendJ Git repository into a local directory named
`extendj`.

Build and Run
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and test the extension:

    gradle jar

    java -jar compiler.jar example/Demo.java
    java -cp example/ Demo

Additional information about the examples and instructions on how to run them can be found in the "example" directory. 

If you do not have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Mac/Linux) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew

The `gradlew` scripts are wrapper scripts that will download Gradle locally and
run it.

File Overview
-------------

Here is a short explanation of the purpose of each file in the project:

* `build.gradle` - the main Gradle build script. There is more info about this below.
*  `jastadd_modules` - this file contains module definitions for the JastAdd build tool. This
  defines things such as which ExtendJ modules to include in the build, and where
additional JastAdd source files are located. Also defines where the parser and scanner specifications can be found.
* `README.md` - this file.
* `gradlew.bat` - Windows Gradle wrapper script (explained above)
* `gradlew` - Unix Gradle wrapper script
* `src/parser/print-assign.beaver` - parser specification for the extension
* `src/scanner/print-assign.flex` - scanner specification for the extension
* `src/jastadd/print-assign.ast` - this file extends the abstract grammar
* `src/jastadd/*.jrag` - aspect files for inter-type declarations. Contains the static analysis and code generation for the extension.
* `testfiles/` - this directory contains the compilation and runtime tests

How this Extension Works
------------------------

This extension builds a compiler that generates calls to "System.out.print" whenever the print-assign operators is used, in addition to assigning.

Primitives and regular objects are passed unproccessed to print, while 1d arrays are called as an argument to Arrays.utils.toString, and arrays of higher dimensions are called as an argument to Arrays.utils.deepToString.

The compiler performs the same static analysis for print-assign as it would for regular assign. Any syntatic violations will prevent class files from being produced.

Additional Resources
--------------------

More examples on how to build ExtendJ-like projects with the [JastAdd Gradle
plugin][2] can be found here:

* [JastAdd Example: GradleBuild](http://jastadd.org/web/examples.php?example=GradleBuild)

[1]:https://gradle.org/
[2]:https://bitbucket.org/joqvist/jastaddgradle/overview
[3]:https://git-scm.com/

Credits
-------

All the files in the repository **EXCEPT** the following originate from the repository *extension-base* which includes ExtendJ and the base framework for building extensions, licensed under BSD 2-clause:

- testfiles/
- src/jastadd/
- src/parser/
- src/scanner/
- src/test/org/extendj/CompileOutputTest.java
- src/test/org/extendj/RunOutputTest.java

License
-------
This repository is covered by the license BSD 2-clause, see file LICENSE.

