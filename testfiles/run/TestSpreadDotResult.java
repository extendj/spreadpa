import java.util.stream.*;
import java.util.Arrays;

public class TestSpreadDotResult {
	static class Person {
		private String name;
		private int ssn;

		private static int nextSSN = 0;

		public Person(String name) {
			this.name = name;
			this.ssn = nextSSN++;
		}

		public String getName() {
			return name;
		}

		public String getName(int what) {
			return "";
		}

		public int getSSN() {
			return ssn;
		}

		public Person getSO() {
			return new Person("Jennifer Annistor");
		}

		public Person getName(String planet) {
			return new Person("Bucklestank Wrinklydonk");
		}

		public Integer getName(String what, int whatwhat) {
			return 42;
		}

		public Object getName(int saywoot, String noway) {
			return new Object();
		}

		public int[] getSomeArr() {
			return new int[] { 0 };
		}

		public String[] getDblArr() [] {
			return new String[][] {{"1", "2", "3"}, {"3", "4"}, {"5"}};
		}

		@Override
		public String toString() {
			return "(" + ssn + ") " + name;
		}
	}

	public static Person[] getPersons() {
		return new Person[] {new Person("hey")};
	}

	public static void main(String[] args) {
		Person[] arr1 = new Person[] {new Person("Blenderdick Cankeryank"),
			new Person("Butawhiteboy Curdlesnatch"),
			new Person("Bandersnatch Chickenstrips")};

		int[][] dbArr .= arr1 *. getSomeArr();
		String[][][] tripArr .= arr1 *. getDblArr();
		String[] names .= arr1 *. getName();
		Person[] sos .= arr1 *. getSO();
		Person[] namesOnPlanet .= arr1 *. getName("Riggidy");
		Integer[] someInt .= arr1 *. getName("Foo", 123);
		// Object[] what .= arr1 *. getName(1234, "Bar"); // Doesn't work in our current testing framework - prints the object ID is not constant
		String[] whatString .= arr1 *. getName(1234);
		String[] sadsadrweb .= getPersons() *. toString();
	}
}
