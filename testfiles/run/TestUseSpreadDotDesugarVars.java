import java.util.stream.*;
import java.util.Arrays;

public class TestUseSpreadDotDesugarVars {
	static class Person {
		private String name;
		private int ssn;

		private static int nextSSN = 0;

		public Person(String name) {
			this.name = name;
			this.ssn = nextSSN++;
		}

		public String getName() {
			return name;
		}

		public String getName(int what) {
			return "";
		}

		public int getSSN() {
			return ssn;
		}

		public Person getSO() {
			return new Person("Jennifer Annistor");
		}

		public Person getName(String planet) {
			return new Person("Bucklestank Wrinklydonk");
		}

		public Integer getName(String what, int whatwhat) {
			return 42;
		}

		public Object getName(int saywoot, String noway) {
			return new Object();
		}

		@Override
		public String toString() {
			return "(" + ssn + ") " + name; 
		}
	}

	public static void main(String[] args) {
		Person[] arr1 = new Person[] {new Person("Blenderdick Cankeryank"),
			new Person("Butawhiteboy Curdlesnatch"),
			new Person("Bandersnatch Chickenstrips")};
		
		int[] ssns = arr1 *. getSSN();
		String[] names .= arr1 *. getName();
		Person[] sos .= arr1 *. getSO();
		Person[] namesOnPlanet .= arr1 *. getName("Riggidy");
		Integer[] someInt .= arr1 *. getName("Foo", 123);
		// Object[] what .= arr1 *. getName(1234, "Bar"); // Doesn't work in our current testing framework - prints the object ID is not constant
		String[] whatString .= arr1 *. getName(1234);

		//Redeclaration of vars used internally in operator desugar
		int[] newArr = new int[1]; 
		int i = 5;
	}
}
