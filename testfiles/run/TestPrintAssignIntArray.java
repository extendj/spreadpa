
public class TestPrintAssignIntArray {
    public static void main(String[] args) {
        int[] a;
        a = new int[2];

        int[] b .= new int[] {3, 2, 9};

        int[] c = new int[] {3, 2, 9};

        a .= c;

        int[][] d .= new int[][] {{1}, {-2, 3}};
        int[][] e .= d;
        d[1][0] = 13 - 11;
        int[][] f;

        f .= e;

        int[][][] g .= new int[][][] {{{1 * 1}, {3 - 1}}, {{6 / 2}}, TestPrintAssignIntArray.getAnArray(), new int[][]{{7, 8}}};
        g[0][0][0] = g[0][0][0] * (-1);
        g .= g;
    }

    public static int[][] getAnArray() {
      return new int[][] {{4, 5}, {18 % 12}};
    }

}
