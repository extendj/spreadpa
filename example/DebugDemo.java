import java.util.*;

public class DebugDemo {
	private int nextId = 0;

	public static void main(String[] args) {
		new DebugDemo();
	}

	public DebugDemo() {
		Family fam = new Family();

		String[] names = new String[] { "Rick Sanchez", "Morty Smith", "Summer Smith", "Beth Smith", "Revolio Clockberg Jr." };

		for (int i = 0; i < names.length; i++) {
			fam.addMember(names[i], getUniqueId());			
		}

		System.out.println(fam);
	}

	private int getUniqueId() {
		//Imagine there is a lot of code here so you dont see this faulty if-statement
		if ((nextId == 0) || (nextId == 4)) {
			nextId++;
			
			return 2;
		}
		
		int id = nextId; //Change this to print-assign to see the Id's
		nextId++;

		return id;
	}
}

class Family {
	private Map<Integer, String> members;

	public Family() {
		this.members = new HashMap<>();
	}

	public void addMember(String name, int uniqueId) {
		members.put(uniqueId, name);
	}

	@Override
	public String toString() {
		return members.values().toString();
	}
}
