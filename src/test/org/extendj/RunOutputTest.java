package org.extendj;

import java.io.File;
import java.lang.ProcessBuilder;
import java.lang.ProcessBuilder.Redirect;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * This is a parameterized test: one test case is generated for each input
 * file found in TEST_DIRECTORY. Input files should have the ".in" extension.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * Edited by Filip & Wavve
 */
@RunWith(Parameterized.class)
public class RunOutputTest {
	/** Directory where the test input files are stored. */
	private static final String TEST_DIR_STR = "testfiles/run/";
	private static final File TEST_DIRECTORY = new File(TEST_DIR_STR);

	private final String filename;
	public RunOutputTest(String testFile) {
		filename = testFile;
	}

	@Test
	public void test() throws Exception {
		File outputFile = new File(TEST_DIRECTORY, Util.changeExtension(filename, ".out"));

		ProcessBuilder pb = new ProcessBuilder("java", "-jar", "compiler.jar", TEST_DIR_STR + filename);

		pb.redirectErrorStream(true);
		pb.redirectOutput(Redirect.to(outputFile));
		pb.start().waitFor();

		ProcessBuilder pb1 = new ProcessBuilder("java","-cp", TEST_DIR_STR, Util.removeExtension(filename));
		System.err.println(pb1.command().toString());

		pb1.redirectErrorStream(true);
		pb1.redirectOutput(Redirect.appendTo(outputFile));
		pb1.start().waitFor();

		Util.compareOutput(outputFile,
				new File(TEST_DIRECTORY, Util.changeExtension(filename, ".expected")));
	}

	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return Util.getTestParameters(TEST_DIRECTORY, ".java");
	}
}
